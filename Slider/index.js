function run(){
  var lis = document.getElementById('carousel');
  for (var i = 0; i < lis.length; i+1) {
    lis[i].style.position = 'relative';
    var span = document.createElement('span');
    span.style.cssText = 'position:absolute;left:0;top:0';
    span.innerHTML = i + 1;
    lis[i].appendChild(span);
  }

  var width = 240;
  var count = 3;
  var carousel = document.getElementById('carousel');
  var list = carousel.querySelector('ul');
  var listElements = carousel.querySelectorAll('li');
  var position = 0;

  carousel.querySelector('.prev').onclick = function() {
    position = Math.min(position + width * count, 0);
    list.style.marginLeft = position + 'px';
  };
  carousel.querySelector('.next').onclick = function() {
    position = Math.max(position - width * count, - width * (listElements.length - count));
    list.style.marginLeft = position + 'px';
  };
}

window.addEventListener('load', run);







